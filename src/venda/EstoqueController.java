package venda;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

//@author kamil
 
public class EstoqueController implements Initializable {

    @FXML
    private TableView<Produto> tabela;
    @FXML
    private TableColumn<?, ?> nomeT;
    @FXML
    private TableColumn<?, ?> quantT;
    @FXML
    private TableColumn<?, ?> precoT;
    @FXML
    private TableColumn<?, ?> codProd;
    @FXML
    private TextField quant;
    @FXML
    private Button botaoAlterar;
    @FXML
    private TableView<Vendaa> tableRelatorio;
    @FXML
    private TableColumn<?, ?> idVenda;
    @FXML
    private TableColumn<?, ?> precoFinal;
    
    private Produto p;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.makeColumns();
        this.makeColumns2();
        this.addItems();
        this.addItems2();
    }    
    @FXML
    private void seleciona(MouseEvent event) {
        p = tabela.getSelectionModel().getSelectedItem();
        quant.setText(""+p.getQuantEstoque());
        botaoAlterar.setDisable(false);
    }
    @FXML
    private void voltar(ActionEvent event) {
        Venda.trocaTela("telaInicial.fxml");
    }
    @FXML
    private void alterar(ActionEvent event) {  
        p = tabela.getSelectionModel().getSelectedItem();
        p.setQuantEstoque(Integer.parseInt(quant.getText()));
        p.update(); 
        tabela.refresh();
    }  
    private void makeColumns() {
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        quantT.setCellValueFactory(new PropertyValueFactory<>("quantEstoque"));
        precoT.setCellValueFactory(new PropertyValueFactory<>("valor"));
        codProd.setCellValueFactory(new PropertyValueFactory<>("idProd"));
    }
    private void makeColumns2() {
        
        idVenda.setCellValueFactory(new PropertyValueFactory<>("idVend")); 
        precoFinal.setCellValueFactory(new PropertyValueFactory<>("valor"));
    }
    private void addItems() {
       for (Produto p2 : Produto.getAll()) {
            tabela.getItems().add(p2);
        }
    }
    private void addItems2() {
       for (Vendaa p2 : Vendaa.getAllVenda()) {
            tableRelatorio.getItems().add(p2);
        }
    }
}
