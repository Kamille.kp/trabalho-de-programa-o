package venda;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

//@author kamil

public class Venda extends Application {
    private static Stage stage;
    public static Stage getStage() {
        return stage;
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(Venda.class.getResource(tela));
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
            System.out.println("Exception " );
            e.printStackTrace();
        }
        
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("telaInicial.fxml"));
        
        Scene scene = new Scene(root);
        //observar essa linha = 
        Venda.stage = stage;
        
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }   
}

