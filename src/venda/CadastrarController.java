package venda;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

//@author kamil
 
public class CadastrarController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private TextField quant;
    @FXML
    private TextField preco;
    @FXML
    private ToggleGroup aleatorio;
    
    private int cont;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    private void CadastrarProduto(ActionEvent event) {
        Produto p = new Produto();    
        p.setNome(nome.getText());
        p.setQuantidade(Integer.parseInt(quant.getText()));
        p.setValor(Double.parseDouble(preco.getText()));
        switch(cont){
            
            case 1:p.setTipo("Alimentação"); break;
            case 2:p.setTipo("Decoração"); break;
            case 3:p.setTipo("Eletrônicos"); break;
            case 4:p.setTipo("Higiene"); break;
            case 5:p.setTipo("Limpeza"); break;
        }
        p.insertProd();
    }
    @FXML
    private void Voltar(ActionEvent event) {
        Venda.trocaTela("telaInicial.fxml");
    }
    @FXML
    private void decoracao(ActionEvent event) {
        cont=2;
    }
    @FXML
    private void alimentacao(ActionEvent event) {
        cont=1;
    }
    @FXML
    private void limpeza(ActionEvent event) {
        cont=5;
    }
    @FXML
    private void higiene(ActionEvent event) {
        cont=4;
    }
    @FXML
    private void eletronicos(ActionEvent event) {
        cont=3;
    }
}
