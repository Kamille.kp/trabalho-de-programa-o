package venda;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javafx.scene.control.Label;

//@author kamil
 
public class ComprarController implements Initializable {

    @FXML
    private TableView<Produto> tableEscolhe;
    @FXML
    private TableColumn<?, ?> nome;
    @FXML
    private TableColumn<?, ?> precoUnico;
    @FXML
    private TableColumn<?, ?> quantEstoque;
    @FXML
    private TextField quantidade;
    @FXML
    private TableView<Produto> tableCarrinho;
    @FXML
    private TableColumn<?, ?> nomeT;
    @FXML
    private TableColumn<?, ?> quantT;
    @FXML
    private TableColumn<?, ?> precoTotal;
    @FXML
    private Button botaoDeletar;
    @FXML
    private Button botaoAdicionar;
    @FXML
    private Label label1;
    
    private ArrayList <Produto> prods = new ArrayList <>();
    private Produto p;
    static Vendaa v;
    private int teste;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.makeColumns();
        this.addItems();
        this.makeColumns2();
        v = new Vendaa();
    }    
    @FXML
    private void adicionarCarrinho(ActionEvent event) {
        p = tableEscolhe.getSelectionModel().getSelectedItem();
        teste = Integer.parseInt(quantidade.getText());
        boolean test = false;
        if(p.getQuantidade() == 0 || teste<1){
            label1.setText("Quantidade inválida");
        }
        else{       
            for(Produto i : tableCarrinho.getItems()) {
                if(i.getNome().equals(p.getNome())) {
                   test = true;
                   i.setQuantidade(i.getQuantidade() + Integer.parseInt(quantidade.getText()));
                }            
            }        
            if(test == false){
               p.setQuantidade(Integer.parseInt(quantidade.getText()));
               tableCarrinho.getItems().add(p);
               prods.add(p);
            }
            
            p.retiraQuant();
            p.update();             
            p.calculaValorTotal(); 
            v.calculaValorVenda(p.getValorTotal());
            tableEscolhe.refresh();
            tableCarrinho.refresh();
        }       
    }
    @FXML
    private void voltar(ActionEvent event) {
        Venda.trocaTela("telaInicial.fxml");
    }
    @FXML
    private void finalizarCompra(ActionEvent event) {
        v.insertVenda();
        int id;
        for(int i=0; i< prods.size(); i++){    
            id = prods.get(i).getIdProd();
            v.insertFinal(id);
            v.setValor(0);
        }   
        tableCarrinho.getItems().clear();
    }
    @FXML
    private void seleciona2(MouseEvent event) {
        botaoDeletar.setDisable(false);
    }
    @FXML
    private void seleciona(MouseEvent event) {
        botaoAdicionar.setDisable(false);
    }
    @FXML
    private void DeletarPorduto(ActionEvent event) {
        p = tableCarrinho.getSelectionModel().getSelectedItem(); 
        tableCarrinho.getItems().remove(p);
        tableEscolhe.refresh();
        prods.remove(p);       
        p.devolveQuant();
        p.update(); 
        p.deletarValorVenda();
    }
    private void makeColumns() {
        nome.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        precoUnico.setCellValueFactory(new PropertyValueFactory<>("valor"));  
        quantEstoque.setCellValueFactory(new PropertyValueFactory<>("quantEstoque"));
    }
    private void makeColumns2() {
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        quantT.setCellValueFactory(new PropertyValueFactory<>("quantidade")); 
        precoTotal.setCellValueFactory(new PropertyValueFactory<>("valorTotal"));   
    }
    private void addItems() {
       for (Produto p2 : Produto.getAll()) {
           tableEscolhe.getItems().add(p2);
        }
    }  
}
