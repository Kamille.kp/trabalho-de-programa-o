package venda;

// @author kamil

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

 
public class Vendaa {
    private int idVend;
    private double valor=0;
    
    public void calculaValorVenda(double total){
        this.valor = this.valor + total;
        System.out.println(this.valor);
    }
    public void DeletarValor(double valorT){
        this.valor = this.valor - valorT;
        System.out.println(this.valor);
    }
    public void insertVenda(){
        Conexao c = new Conexao(); 
        String insertTableSQL;
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        String newId[] = {"id_vendas"};        
        try{
            insertTableSQL = "insert into VEND (id_vendas, valorTotal) VALUES (Vendaid.nextval, ?)";
            ps = dbConnection.prepareStatement(insertTableSQL, newId);
            
            ps.setDouble(1, this.valor);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                idVend = rs.getInt(1);
            }
        }  
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void insertFinal(int idProd){
        Conexao c = new Conexao(); 
        String insertTableSQL;
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        try{
            insertTableSQL = "insert into ITEMV (id, id_venda, id_prod) VALUES (itemvid.nextval, ?, ?)";
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setInt(1, this.idVend);
            preparedStatement.setInt(2, idProd);
            preparedStatement.executeUpdate();
        }  
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public static Iterable<Vendaa> getAllVenda() {
        ArrayList <Vendaa> lista = new ArrayList<Vendaa>();
        String newId[] = {"id_vendas"}; 
        try{
            Conexao c = new Conexao();
            Connection  dbConnection = c.getConexao();     
            String selectSQL = "SELECT * FROM vend";
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Vendaa vend = new Vendaa();
                vend.setIdVend(rs.getInt(1));
                vend.setValor(rs.getInt("ValorTotal"));
                lista.add(vend);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        System.out.println(lista);
        return lista;
    }
    
    public int getIdVend() {
        return idVend;
    }
    public void setIdVend(int idVend) {
        this.idVend = idVend;
    }
    public double getValor() {
        return valor;
    }
    private void setValor(int v){
        this.valor = v;    
    }
    public void setValor(double valor) {
        this.valor = valor;
    }    
}
