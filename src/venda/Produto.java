package venda;

// @author kamille

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

 
public class Produto {
    private int idProd, quantidade=0, quantEstoque=0, cont=0;
    private String nome, tipo;
    private double valor, valorTotal=0;
    
    //valor
    public double calculaValorTotal(){  
        this.valorTotal =  this.quantidade * this.valor;
        return valorTotal;
    }
    public void deletarValorVenda(){
        ComprarController.v.DeletarValor(this.getValorTotal());
    }
    //quantidade
    public void retiraQuant(){
        quantEstoque = quantEstoque - quantidade;
        System.out.println(quantEstoque);
    }
    public void devolveQuant(){
        quantEstoque = quantEstoque + quantidade;
        System.out.println(quantEstoque);
    }
    //crud
    public static Iterable<Produto> getAll() {
        ArrayList <Produto> lista = new ArrayList<Produto>();
        try{
            Conexao c = new Conexao();
            Connection  dbConnection = c.getConexao();     
            String selectSQL = "SELECT * FROM PROD";
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Produto p = new Produto();
                p.setNome(rs.getString("nome"));
                p.setQuantEstoque(rs.getInt("quantidade"));
                p.setValor(rs.getInt("valor"));
                p.setIdProd(rs.getInt("id_prod"));
                lista.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        System.out.println(lista);
        return lista;
    }
    public void insertProd(){
        Conexao c = new Conexao(); 
        String insertTableSQL;
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        try{
            insertTableSQL = "insert into prod (id_prod, nome, quantidade, valor, tipo) VALUES (produtoid.nextval, ?, ?, ?, ?)";
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setString(1, this.nome);
            preparedStatement.setInt(2, this.quantidade);
            preparedStatement.setDouble(3, this.valor);
            preparedStatement.setString(4, this.tipo);            
            preparedStatement.executeUpdate();
        }  
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void update(){
        Conexao c = new Conexao();
        Connection con = c.getConexao();
        
        String sql = "UPDATE PROD SET quantidade = ? WHERE id_prod = ?";
        try{
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setInt(1, this.quantEstoque);
            ps.setInt(2, this.idProd);
            ps.executeUpdate();   
            
            
        }
        catch(SQLException e){            
            e.printStackTrace();
        }
    }
    public static Iterable<Produto> getOne(){
        ArrayList <Produto> lista = new ArrayList<Produto>();
        try{
            Conexao c = new Conexao();
            Connection  dbConnection = c.getConexao();     
            String selectSQL = "SELECT * FROM PROD";
            Statement st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Produto p = new Produto();
                p.setIdProd(rs.getInt("id_prod"));
                lista.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        System.out.println(lista);
        return lista;
    }
    
    //Getters e Setters
    public int getIdProd() {
        return idProd;
    }
    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }  
    public double getValorTotal() {
        return valorTotal;
    }
    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }
    public int getCont() {
        return cont;
    }
    public void setCont(int cont) {
        this.cont = cont;
    }  
    public int getQuantEstoque() {
        return quantEstoque;
    }
    public void setQuantEstoque(int quantEstoque) {
        this.quantEstoque = quantEstoque;
    }        
}
