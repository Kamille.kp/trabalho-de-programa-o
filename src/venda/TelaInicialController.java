package venda;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

// @author kamil
 
public class TelaInicialController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    private void telaCadastrar(ActionEvent event) {
        Venda.trocaTela("cadastrar.fxml");
    }
    @FXML
    private void telaComprar(ActionEvent event) {
         Venda.trocaTela("comprar.fxml");
    }
    @FXML
    private void telaEstoque(ActionEvent event) {
         Venda.trocaTela("estoque.fxml");
    }
    
}
